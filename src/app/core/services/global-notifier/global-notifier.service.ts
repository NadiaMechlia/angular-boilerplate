import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertSnackbarComponent } from 'src/app/shared/components/alert-snackbar/alert-snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class GlobalNotifierService {

  constructor(private _snackBar: MatSnackBar, private zone: NgZone) { }

  displayErrorNotif(message: string) {
    this.zone.run(() => {
      this._snackBar.openFromComponent(AlertSnackbarComponent, {
        duration: 3 * 1000,
        horizontalPosition: 'right',
        data: message
      });
    });
  }
}
