import { TestBed } from '@angular/core/testing';

import { GlobalNotifierService } from './global-notifier.service';

describe('GlobalNotifierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalNotifierService = TestBed.get(GlobalNotifierService);
    expect(service).toBeTruthy();
  });
});
