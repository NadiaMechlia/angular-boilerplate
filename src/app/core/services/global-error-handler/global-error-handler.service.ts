import { Injectable, ErrorHandler } from '@angular/core';
import { GlobalNotifierService } from '../global-notifier/global-notifier.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandlerService implements ErrorHandler {
  private readonly DEFAULT_ERROR_TITLE: string = 'Oups ! Une erreur interne s\'est produite';
  constructor(private globalNotifServ: GlobalNotifierService) {}

  public handleError(error: any) {
    // display notification to let user know an error happened
    if (error instanceof HttpErrorResponse) {
    const errorHttpCode = error.status;
    switch (errorHttpCode) {
      case 500:
        this.globalNotifServ.displayErrorNotif(this.DEFAULT_ERROR_TITLE);
        break;
      case 0:
        this.globalNotifServ.displayErrorNotif(this.DEFAULT_ERROR_TITLE);
        break;
      default:
        console.error('A http error occured :', error);
        break;
    }
    } else {
      this.globalNotifServ.displayErrorNotif(this.DEFAULT_ERROR_TITLE);
      console.error('An error occured :', error);
    }
  }
}
