import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './public/signin/signin.component';
import { ResetPasswordComponent } from './public/reset-password/reset-password.component';
import { NotFoundComponent } from './public/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'backoffice'
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'reset',
    component: ResetPasswordComponent
  },
  {
    path: 'backoffice',
    loadChildren: 'src/app/backoffice/backoffice.module#BackofficeModule'
  },
  {
    path: 'notfound',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/notfound'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
