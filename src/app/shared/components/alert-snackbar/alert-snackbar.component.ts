import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-alert-snackbar',
  templateUrl: './alert-snackbar.component.html',
  styleUrls: ['./alert-snackbar.component.scss']
})
export class AlertSnackbarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public alertMessage: any) { }

  ngOnInit() {
  }

}
