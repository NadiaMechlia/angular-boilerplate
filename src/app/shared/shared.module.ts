import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertSnackbarComponent } from './components/alert-snackbar/alert-snackbar.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [AlertSnackbarComponent],
  imports: [
    CommonModule,

    // Material modules
    MatSnackBarModule
  ]
})
export class SharedModule { }
